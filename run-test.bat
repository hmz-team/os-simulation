@echo off

echo ===========================================================================
echo Running test: %1
echo ===========================================================================

mkdir compiled > nul 2> nul
xcopy /y %1 compiled > nul

cd compiled

type "test-input.txt" | find /v "" | boot.exe
REM this also converts lf to crlf

cd ..

#include "parser.h"
#include "utils.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <regex>

size_t split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
	size_t pos = txt.find(ch);
	size_t initial_pos = 0;
	strs.clear();
	char cs, ce;
	int start_of_arg = -1;
	int end_of_arg = -1;
	bool arg_last = false;

	// Decompose statement
	while (pos != std::string::npos) {
		cs = txt.at(initial_pos);
		ce = txt.at(pos - 1);
		arg_last = false;

		if (txt.substr(initial_pos, FIND.size()) == FIND) {
			strs.push_back(FIND);
			initial_pos += FIND.length();
			pos = txt.find(ch, initial_pos);
			arg_last = true;
			continue;
		}

		if (cs == '\"') {
			start_of_arg = (int)initial_pos + 1;
		}

		else if (ce == '\"') {
			end_of_arg = pos - 1;

			if (start_of_arg > -1 && end_of_arg > -1) {
				strs.push_back(txt.substr(start_of_arg, end_of_arg - start_of_arg));
			}

			start_of_arg = end_of_arg = -1;
			arg_last = true;
		}

		else if (start_of_arg == -1 && end_of_arg == -1) {
			strs.push_back(txt.substr(initial_pos, pos - initial_pos));

			if (txt.find(ch, pos + 1) == std::string::npos && pos < txt.length() && txt.at(pos + 1) == '\"') {
				start_of_arg = pos + 2;
				end_of_arg = txt.length() - 1;
				strs.push_back(txt.substr(start_of_arg, end_of_arg - start_of_arg));
				arg_last = true;
				break;
			}
		}

		initial_pos = pos + 1;

		pos = txt.find(ch, initial_pos);

		if (pos == std::string::npos && start_of_arg != -1) {
			pos = txt.size();
		}
	}

	if (!arg_last) {
		strs.push_back(txt.substr(initial_pos, std::min(pos, txt.size()) - initial_pos + 1));
	}

	return strs.size();
}

std::vector<parser::command*> parser::parse_commands(char *buffer)
{
	std::vector<parser::command*> vec{};

	std::string line{ buffer };
	line = trim(line);

	std::vector<std::string> strs{};
	size_t strs_size = split(line, strs, ' ');

	std::regex regex = std::regex("([|<>])");

	for (size_t i = 0; i < strs_size;) {
		parser::command* com = new parser::command();

		com->command = strs[i];
		com->out = parser::StreamType::STD;
		com->params = "";
		com->file_name = "";

		if (i != 0 && strs[i - 1].compare(PIPE) == 0) {
			com->in = parser::StreamType::PIPE;
		} 
		
		else {
			com->in = parser::StreamType::STD;
		}

		if (i + 1 == strs_size) {
			vec.push_back(com);
			break;
		}

		if (std::regex_match(strs[i + 1], regex)) {
			if (strs[i + 1].compare(PIPE) == 0) {
				com->out = parser::StreamType::PIPE;

				i += 2;
			}

			else if (i + 1 < strs.size() && strs[i + 1].compare(REDIR_OUT) == 0) {
				com->out = parser::StreamType::FILE;
				com->file_name = strs[i + 2];

				i += 3;
			}

			else
			{
				com->in = parser::StreamType::FILE;
				com->file_name = strs[i + 2];

				i += 4;
			}
		}

		else {
			com->params = strs[i + 1];

			if (i + 2 >= strs.size()) {
				vec.push_back(com);
				break;
			}

			if (strs[i + 2].compare(PIPE) == 0) {
				com->out = parser::StreamType::PIPE;

				i += 3;
			}

			else {
				com->out = parser::StreamType::FILE;
				if (i + 3 >= strs.size()) {
					//todo
				}
				com->file_name = strs[i + 3];

				i += 4;
			}
		}

		vec.push_back(com);
	}

	return vec;
}

void parser::free_commands(std::vector<parser::command*> vec)
{
	for (auto it = vec.begin(); it != vec.end(); ++it) {
		delete *it;
	}
}
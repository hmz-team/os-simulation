#include "shutdown.h"
#include "rtl.h"

size_t __stdcall shutdown(const kiv_hal::TRegisters &regs) { 
	
	kiv_os_rtl::Shutdown();
	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);

	return 0; 
}
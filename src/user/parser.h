#pragma once

#include <vector>
#include <string>

namespace parser {

	enum class StreamType {
		STD,
		PIPE,
		FILE
	};

	struct command {
		std::string command;
		std::string params;
		std::string file_name;
		StreamType in;
		StreamType out;
	};

	std::vector<parser::command*> parse_commands(char* buffer);
	void free_commands(std::vector<parser::command*> vec);
}
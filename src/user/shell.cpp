#include "shell.h"
#include "parser.h"
#include "utils.h"
#include "leaks.h"

#include "rtl.h"

bool run = true;

void print_error(const char* msg, kiv_os::THandle out) {
	size_t counter;

	kiv_os_rtl::Write_File(out, msg, strlen(msg), counter);
}

const char* resolve_error(kiv_os::NOS_Error err) {
	switch (err) {

	case kiv_os::NOS_Error::Directory_Not_Empty:
		return DIRECTORY_NOT_EMPTY;

	case kiv_os::NOS_Error::File_Not_Found:
		return FILE_NOT_FOUND;

	case kiv_os::NOS_Error::Invalid_Argument:
		return INVALID_ARG;

	case kiv_os::NOS_Error::IO_Error:
		return IO_ERROR;

	case kiv_os::NOS_Error::Not_Enough_Disk_Space:
		return NOT_ENOUGH_DISK_SPACE;

	case kiv_os::NOS_Error::Out_Of_Memory:
		return OUT_OF_MEMORY;

	case kiv_os::NOS_Error::Permission_Denied:
		return PERMISSION_DENIED;

	default:
		return UNKNOWN_ERROR;
	}
}

bool start_program(parser::command* com, kiv_os::THandle out_handle) {
	kiv_os::THandle ret = kiv_os::Invalid_Handle;

	kiv_os::THandle in = 0;
	kiv_os::THandle out = 0;

	bool read_exit_code = true;

	if (com->in == parser::StreamType::STD) {
		in = 0;
	}

	//TODO other input types

	if (com->out == parser::StreamType::STD) {
		out = 1;
	}

	//TODD other output types

	if (com->command.compare(ECHO) == 0) {
		ret = kiv_os_rtl::Create_Process(com->command.c_str(), com->params.c_str(), in, out);
	}

	else if (com->command.compare(SHUTDOWN) == 0) {
		ret = kiv_os_rtl::Create_Process(com->command.c_str(), in, out);
		read_exit_code = false;
	}

#ifdef _DEBUG
	else if (com->command.compare(NTFS_DEBUG_COMMAND) == 0) {
		kiv_os_rtl::NTFS_Debug_Command(com->params.c_str());
		read_exit_code = false;
	}
#endif

	else {
		ret = kiv_os::Invalid_Handle;
	}

	if (ret == kiv_os::Invalid_Handle) {
		print_error(PROC_ERR_NOT_FOUND, out_handle);
		return false;
	}

	if (read_exit_code) {
		kiv_os::NOS_Error exit_code = static_cast<kiv_os::NOS_Error>(kiv_os_rtl::Read_Exit_Code(ret));


		if (exit_code != kiv_os::NOS_Error::Success) {
			print_error(resolve_error(exit_code), out_handle);
			return false;
		}
	}

	return true;
}

size_t __stdcall handle_term(const kiv_hal::TRegisters &regs) {
	run = false;
	return 0;
}

size_t __stdcall shell(const kiv_hal::TRegisters &regs) {

	/*
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);
	*/

	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(0);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(1);

	kiv_os_rtl::Signals_Register(handle_term, kiv_os::NSignal_Id::Terminate);

	const size_t buffer_size = 256;
	char buffer[buffer_size];
	size_t counter;

	const char* prompt = "C:\\>";
	do {
		kiv_os_rtl::Write_File(std_out, prompt, strlen(prompt), counter);

		if (kiv_os_rtl::Read_File(std_in, buffer, buffer_size, counter) && (counter > 0)) {
			if (counter == buffer_size) counter--;
			buffer[counter] = 0;	//udelame z precteneho vstup null-terminated retezec

			const char* new_line = "\n";
			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);

			auto commands = parser::parse_commands(buffer);

			int i = 0;
			for (auto & com : commands) {
				bool ret = start_program(com, std_out);

				if (!ret) {
					break;
				}
				//TODO pokud je pipe tak pokracuj ve spusteni dalsiho, dokud se nedojde na konec
				//zretezenych programu a ceka se az na ukonceni posledniho nebo tak nejak...
			}

			parser::free_commands(commands);
			
			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);
		}
		else
			break;	//EOF
	} while (run);

	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);

	return 0;	
}
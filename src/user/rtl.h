#pragma once

#include "..\api\api.h"
#include "parser.h"
#include <atomic>

namespace kiv_os_rtl {

	extern std::atomic<kiv_os::NOS_Error> Last_Error;

	bool Read_File(const kiv_os::THandle file_handle, char* const buffer, const size_t buffer_size, size_t &read);
		//zapise do souboru identifikovaneho deskriptor data z buffer o velikosti buffer_size a vrati pocet zapsanych dat ve written
		//vraci true, kdyz vse OK
		//vraci true, kdyz vse OK

	bool Write_File(const kiv_os::THandle file_handle, const char *buffer, const size_t buffer_size, size_t &written);
	//zapise do souboru identifikovaneho deskriptor data z buffer o velikosti buffer_size a vrati pocet zapsanych dat ve written
	//vraci true, kdyz vse OK
	//vraci true, kdyz vse OK

	bool Create_Process(const char* process, kiv_os::THandle in, kiv_os::THandle out);

	kiv_os::THandle Create_Process(const char* process, const char* arg, kiv_os::THandle in, kiv_os::THandle out);

	bool Create_Thread(void* data, kiv_os::TThread_Proc proc, kiv_os::THandle in, kiv_os::THandle out);

	bool Signals_Register(kiv_os::TThread_Proc handler, kiv_os::NSignal_Id id);

	bool Shutdown();

	bool Exit(kiv_os::NOS_Error exit_code);

	kiv_os::NOS_Error Read_Exit_Code(kiv_os::THandle handle);

	bool NTFS_Debug_Command(const char* command);

	bool Open_File(const char* filename, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attrs);

	bool Seek(const kiv_os::THandle file_handle, const uint64_t new_position, kiv_os::NFile_Seek position_type);

	bool Close_Handle(const kiv_os::THandle file_handle);

	bool Delete_File(const char* filename);

	bool Set_Working_Dir(const char* dirname);

	bool Get_Working_Dir(const char* dirname, const uint64_t buffer_size);

	bool Create_Dir(const char* dirname);

	bool Delete_Dir(const char* dirname);

	bool Read_Dir(const char* dirname);

	bool Create_Pipe(const kiv_os::THandle* handlers_array);
}
#include "echo.h"

#include "rtl.h"

size_t __stdcall test_thread(const kiv_hal::TRegisters &regs) {
	size_t counter;

	const kiv_os::THandle in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out = static_cast<kiv_os::THandle>(regs.rbx.x);
	void* data = reinterpret_cast<void*>(regs.rdi.r);

	const char* text = reinterpret_cast<const char* const>(data);
	kiv_os_rtl::Write_File(out, text, strlen(text), counter);

	return 0;
}

size_t __stdcall echo(const kiv_hal::TRegisters &regs) {

	const kiv_os::THandle in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out = static_cast<kiv_os::THandle>(regs.rbx.x);

	//kiv_os_rtl::Create_Thread(static_cast<void *>("test thread\n"), test_thread, 0, 1);

	if (in == 0) { //STDIN - data jsou v argumentu
		size_t counter;

		char* arg = reinterpret_cast<char*>(regs.rdi.r);

		if (strcmp(arg, "") == 0) {
			kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
			return 0;
		}

		kiv_os_rtl::Write_File(out, arg, strlen(arg), counter);

	}

	else { 
		
		//TODO nacteni dat ze souboru/daneho handleu

	}

	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);

	return 0;	
}
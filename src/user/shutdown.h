#pragma once
#include "../api/api.h"

size_t __stdcall shutdown(const kiv_hal::TRegisters &regs);
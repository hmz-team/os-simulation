#include "rtl.h"

std::atomic<kiv_os::NOS_Error> kiv_os_rtl::Last_Error;

kiv_hal::TRegisters Prepare_SysCall_Context(kiv_os::NOS_Service_Major major, uint8_t minor) {
	kiv_hal::TRegisters regs;
	regs.rax.h = static_cast<uint8_t>(major);
	regs.rax.l = minor;
	return regs;
}

bool kiv_os_rtl::Read_File(const kiv_os::THandle file_handle, char* const buffer, const size_t buffer_size, size_t &read) {
	kiv_hal::TRegisters regs =  Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Read_File));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = buffer_size;	

	const bool result = kiv_os::Sys_Call(regs);
	read = regs.rax.r;
	return result;
}

bool kiv_os_rtl::Write_File(const kiv_os::THandle file_handle, const char *buffer, const size_t buffer_size, size_t &written) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Write_File));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = buffer_size;

	const bool result = kiv_os::Sys_Call(regs);
	written = regs.rax.r;
	return result;
}

bool kiv_os_rtl::Create_Process(const char * process, kiv_os::THandle in, kiv_os::THandle out) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
	regs.rbx.e = (in << 16) | out;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(kiv_os::NClone::Create_Process);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(process);

	const bool result = kiv_os::Sys_Call(regs);

	return result;
}

kiv_os::THandle kiv_os_rtl::Create_Process(const char * process, const char* arg, kiv_os::THandle in, kiv_os::THandle out) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
	regs.rbx.e = (in << 16) | out;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(kiv_os::NClone::Create_Process);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(process);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(arg);

	kiv_os::Sys_Call(regs);

	kiv_os::THandle result = regs.rax.x;

	return result;
}

bool kiv_os_rtl::Create_Thread(void * data, kiv_os::TThread_Proc proc, kiv_os::THandle in, kiv_os::THandle out)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
	regs.rbx.e = (in << 16) | out;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(kiv_os::NClone::Create_Thread);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(proc);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(data);

	const bool result = kiv_os::Sys_Call(regs);

	return result;
}

bool kiv_os_rtl::Signals_Register(kiv_os::TThread_Proc handler, kiv_os::NSignal_Id id)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Register_Signal_Handler));
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(id);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(handler);

	const bool result = kiv_os::Sys_Call(regs);

	return result;
}

bool kiv_os_rtl::Shutdown()
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Shutdown));

	bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Exit(kiv_os::NOS_Error exit_code)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Exit));
	regs.rcx.x = static_cast<decltype(regs.rcx.x)>(exit_code);

	bool result = kiv_os::Sys_Call(regs);
	return result;
}

kiv_os::NOS_Error kiv_os_rtl::Read_Exit_Code(kiv_os::THandle handle)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Read_Exit_Code));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(handle);
	kiv_os::Sys_Call(regs);

	return static_cast<kiv_os::NOS_Error>(regs.rcx.x);
}

bool kiv_os_rtl::NTFS_Debug_Command(const char* command) {
	kiv_hal::TRegisters regs;
	regs.rax.r = 3;
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(command);

	return kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Open_File(const char* filename, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attrs) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Open_File));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(filename);
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(flags);
	regs.rdi.r = static_cast<decltype(regs.rdi.r)>(attrs); // pripadne zmenit na pozadovanou velikost registru, dal jsem 64, ale attrs jsou 8bitovy

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Seek(const kiv_os::THandle file_handle, const uint64_t new_position, kiv_os::NFile_Seek position_type) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Seek));
	regs.rdx.x = static_cast<decltype(regs.rdx.r)>(file_handle);
	regs.rdi.r = static_cast<decltype(regs.rdi.r)>(new_position);
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(position_type);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Close_Handle(const kiv_os::THandle file_handle) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Close_Handle));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Delete_File(const char* filename) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Delete_File));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(filename);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Set_Working_Dir(const char* dirname) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Set_Working_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(dirname);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Get_Working_Dir(const char* dirname, const uint64_t buffer_size) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Get_Working_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(dirname);
	regs.rcx.r = static_cast<decltype(regs.rdx.r)>(buffer_size);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Create_Dir(const char* dirname) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Create_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(dirname);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Delete_Dir(const char* dirname) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Delete_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(dirname);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Read_Dir(const char* dirname) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Read_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(dirname);

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Create_Pipe(const kiv_os::THandle* handlers_array) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Create_Pipe));
	regs.rdx.e = (handlers_array[0] << 16) | handlers_array[1];

	const bool result = kiv_os::Sys_Call(regs);
	return result;
}
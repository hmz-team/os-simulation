#pragma once

#include "kernel.h"
#include "io.h"
#include "process_management.h"
#include "disk_management.h"
#include <Windows.h>
#include <memory>
#include <stdio.h>
#include "../user/leaks.h"

HMODULE User_Programs;
std::shared_ptr<Process_Management> process_management = 0;
std::shared_ptr<Disk_Management> disk_management = 0;

void Initialize_Kernel() {
	User_Programs = LoadLibraryW(L"user.dll");
	process_management = std::make_shared<Process_Management>();
}

void Shutdown_Kernel() {
	FreeLibrary(User_Programs);
}

void __stdcall Sys_Call(kiv_hal::TRegisters &regs) {

	switch (static_cast<kiv_os::NOS_Service_Major>(regs.rax.h)) {
	
		case kiv_os::NOS_Service_Major::File_System:		
			Handle_IO(regs);
			break;

		case kiv_os::NOS_Service_Major::Process:
			process_management->handle_process(regs, User_Programs);
			break;

		default:
#ifdef _DEBUG
			if (regs.rax.r == 3) {
				// NTFS debug command
				if (disk_management) {
					char* command = reinterpret_cast<char*>(regs.rdx.r);
					disk_management->Run_Debug_Command(command);
					break;
				}
			}
#endif
			// TODO: vr�tit chybu - nezn�m� slu�ba
			break;
	}

}

void __stdcall Bootstrap_Loader(kiv_hal::TRegisters &context) {
	Initialize_Kernel();
	kiv_hal::Set_Interrupt_Handler(kiv_os::System_Int_Number, Sys_Call);

	//v ramci ukazky jeste vypiseme dostupne disky
	kiv_hal::TRegisters regs;
	for (regs.rdx.l = 0; ; regs.rdx.l++) {
		kiv_hal::TDrive_Parameters params;		
		regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Drive_Parameters);
		regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(&params);
		kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);

		if (!regs.flags.carry && params.absolute_number_of_sectors != 36028797018963967) {
			disk_management = std::make_shared<Disk_Management>(regs.rdx.l, params);
			break;  // TODO: m�me podporovat v�ce disk�?
		}

		if (regs.rdx.l == 255) break;
	}

	/*
	//spustime shell - v realnem OS bychom ovsem spousteli login
	kiv_os::TThread_Proc shell = (kiv_os::TThread_Proc)GetProcAddress(User_Programs, "shell");
	if (shell) {
		//spravne se ma shell spustit pres clone!
		//ale ten v kostre pochopitelne neni implementovan		
		shell(regs);
	}
	*/

	//jak pockat na konec shellu?
	kiv_hal::TRegisters registers;
	const kiv_os::THandle in = static_cast<kiv_os::THandle>(0);
	const kiv_os::THandle out = static_cast<kiv_os::THandle>(1);
	registers.rbx.e = (in << 16) | out;
	registers.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>("shell");
	process_management->create_process(registers, User_Programs);
	process_management->wait_for_first_program();

	Shutdown_Kernel();
}


void Set_Error(const bool failed, kiv_hal::TRegisters &regs) {
	if (failed) {
		regs.flags.carry = true;
		regs.rax.r = GetLastError();
	}
	else
		regs.flags.carry = false;
}
#pragma once

/**
 * Definuje funkce, které umožňují alokovat a uvolňovat různé zdroje.
 *
 * @author: Patrik Harag
 * @version: 2018-10-27
 */

#include "ntfs.h"


extern bool DISABLE_FIRST_FIT;

int32_t ntfs_extend_file(NTFS *ntfs, MftItem *first_item, int32_t bytes);
int32_t ntfs_extend_file_last_cluster(NTFS *ntfs, MftItem *first_item, int32_t bytes);
int32_t ntfs_extend_file_last_fragment(NTFS *ntfs, MftItem *first_item, int32_t bytes, MftFragment *last_non_empty_fragment);
int32_t ntfs_extend_file_using_new_fragments(NTFS *ntfs, MftItem *first_item, int32_t bytes, MftFragment *last_non_empty_fragment);

bool ntfs_trim_file(NTFS *ntfs, MftItem *first_item, int32_t target_size_in_bytes);

/** Najde určitý počet volných clusterů. Nemění bitmapu! */
MftFragment* ntfs_find_free_clusters(NTFS *ntfs, int32_t n, int32_t preferred_min_cluster_index, int32_t *fragment_count);
/** Alokuje clustery podle zadané velikosti */
bool ntfs_allocate_clusters(NTFS *ntfs, long size_bytes_l,
	   int32_t preferred_min_cluster_index, MftFragment** out_fragments, int32_t* out_fragment_count);
/** Uvolní clustery */
void ntfs_release_clusters(NTFS *ntfs, MftFragment* fragments, int count);

/** Alokuje MFT položky */
bool ntfs_allocate_mft_items(NTFS *ntfs, int32_t n, MftItem** out_items, int32_t uid);
/** Uvolní MFT položky */
void ntfs_release_mft_items(NTFS *ntfs, MftItem** items, int32_t n);

#pragma once

/**
 * Definuje funkce pro práci s IO.
 *
 * @author: Patrik Harag
 * @version: 2018-10-27
 */

#include "ntfs.h"

/** Naformátuje soubor s ntfs */
bool io_disk_format(NTFS *ntfs);
/** Načte informace o ntfs ze souboru - boot record, mft, bitmap */
bool io_disk_try_load(NTFS *ntfs);

/** Načte data souboru ve ntfs a uloží je do bufferu */
int32_t io_file_read(NTFS* ntfs, MftItem* first_item, int32_t file_offset, void* out_buffer, int32_t out_buffer_size);
char* io_file_read_all(NTFS* ntfs, MftItem* first_item);

/** Zapíše data z bufferu do souboru */
int32_t io_file_write(NTFS* ntfs, MftItem* first_item, int32_t file_offset, void* data, int32_t data_size);
int32_t io_file_write_append(NTFS* ntfs, MftItem* first_item, void* data, int32_t data_size);
/** Zkopíruje data z clusteru na určenou pozici */
bool io_copy_cluster(NTFS* ntfs, int32_t src_cluster_index, int32_t dest_cluster_index);

/** Zapíše boot record do souboru */
bool io_update_boot_record(NTFS* ntfs);
/** Zapíše MFT do souboru */
bool io_update_mft(NTFS* ntfs);
/** Zapíše bitmapu do souboru */
bool io_update_bitmap(NTFS* ntfs);

/** Vrátí index, kde v souboru začíná sekce s MFT */
int32_t io_get_mft_section_start(NTFS* ntfs);
/** Vrátí index, kde v souboru začíná sekce s bitmapou */
int32_t io_get_bitmap_section_start(NTFS* ntfs);
/** Vrátí index, kde v souboru začíná sekce s daty */
int32_t io_get_data_section_start(NTFS* ntfs);

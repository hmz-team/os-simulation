#pragma once

/**
 * Definice všech příkazů.
 *
 * @author: Patrik Harag
 * @version: 2018-10-27
 */

#include "ntfs.h"
#include "ntfs-utils.h"
#include "ntfs-console.h"

/** Inicializuje virtuální fs */
Result command_init(ConsoleState* console_state, char** argv, int argc);

/** Vypíše informace o obsazenosti fs */
Result command_df(NTFS* ntfs);
/** Vypíše parametry fs */
Result command_show_parameters(NTFS* ntfs);
/** Zobrazí bitmapu; 0 => volný cluster, 1 => obsazený cluster */
Result command_show_bitmap(NTFS* ntfs);
/** Vypíše MFT položky */
Result command_show_mft(NTFS* ntfs);

/** Vypíše aktuální cestu */
Result command_pwd(ConsoleState* console_state);
/** Změní aktuální cestu */
Result command_cd(ConsoleState* console_state, char* path);
/** Vypíše seznam položek v adresáři */
Result command_ls(ConsoleState* console_state, char* path);
/** Vytvoří nový adresář */
Result command_mkdir(ConsoleState* console_state, char* path);
/** Smaže adresář */
Result command_rmdir(ConsoleState* console_state, char* path);
/** Nahraje soubor do virtuálního fs */
Result command_incp(ConsoleState* console_state, char* src_path, char* dest_path);
/** Smaže soubor */
Result command_rm(ConsoleState* console_state, char* path);
/** Vytvoří prázdný soubor */
Result command_create(ConsoleState* console_state, char* dest_path);
/** Vypíše informace o položce */
Result command_info(ConsoleState* console_state, char* path);
/** Vypíše obsah souboru */
Result command_read(ConsoleState* console_state, char* path, int32_t offset, int32_t size);
/*  Zapíše do souboru */
Result command_write(ConsoleState* console_state, char* path, int32_t offset, char *data, int32_t data_size);

/** Realokuje soubor na jinou pozici - to ho může defragmentovat */
Result command_reallocate(ConsoleState* console_state, char* path);
/** Defragmentuje celý virtuální fs */
Result command_defragment(ConsoleState* console_state);

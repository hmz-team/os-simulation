
/**
 * Implementuje funkce pro defragmentaci.
 *
 * @author: Patrik Harag
 * @version: 2018-10-28
 */

#include <stdlib.h>
#include <assert.h>
#include "ntfs-module-defragmentation.h"
#include "ntfs-module-allocation.h"
#include "ntfs-module-utils.h"
#include "ntfs-module-find.h"
#include "ntfs-io.h"
#include "ntfs-debug.h"


/** Najde první neprázdný cluster */
static int32_t ntfs_defragment_find_first_cluster(
		NTFS* ntfs, MftItem** lookup_table, int32_t start) {

	for (int i = start; i < ntfs->bitmap->cluster_count; ++i) {
		if (lookup_table[i] != NULL)
			return i;
	}
	return -1;
}

Result ntfs_defragment(NTFS* ntfs) {
	// vytvoření tabulky clusterů: index => MftItem*
	MftItem **lookup_table = (MftItem**) malloc((ntfs->bitmap->cluster_count) * sizeof(MftItem*));
	ntfs_lookup_table_fill(ntfs, lookup_table);

	int32_t next_cluster_to_process = 0;
	while (true) {

		// najdeme první neprázdný cluster a podle něj určíme další položku,
		// kterou budeme defragmetovat
		int32_t next_non_empty_cluster = ntfs_defragment_find_first_cluster(
				ntfs, lookup_table, next_cluster_to_process);

		if (next_non_empty_cluster < 0) {
			// defragmentováno
			break;
		}

		// během defragmetace této položky budou informace o jejích clusterech
		// uložené v mft neaktuální!
		MftItem* next_item = lookup_table[next_non_empty_cluster];
		int32_t next_item_index = ntfs_find_item_index_if_needed(ntfs, next_item);
		int32_t next_item_size = next_item->item_size;
		int32_t next_item_cluster_count = ntfs_count_clusters(ntfs, next_item);
		int32_t *next_item_clusters = (int32_t*) malloc((next_item_cluster_count) * sizeof(int32_t));
		ntfs_collect_clusters_from_mft_items(ntfs, next_item, next_item_index, next_item_clusters);

		if (next_cluster_to_process == next_non_empty_cluster
				&& ntfs_mft_is_not_fragmented(next_item)) {

			// už je to defragmentované...
			next_cluster_to_process += next_item_cluster_count;

			free(next_item_clusters);
			continue;
		}

		// půjdeme cluster po clusteru a budeme je přesouvat na správné místo
		for (int i = 0; i < next_item_cluster_count; ++i) {
			int32_t dest_cluster = next_cluster_to_process + i;
			MftItem* dest_current_item = lookup_table[dest_cluster];

			if (dest_current_item != NULL) {
				// někdo tam je

				if (dest_current_item->uid == next_item->uid) {
					// jsou tam správná data..
					continue;

				} else {
					// zajistíme, aby byl cluster uvolněn
					// - realokujeme celý soubor někam pryč

					int32_t dest_current_item_index = ntfs_find_item_index_if_needed(
							ntfs, dest_current_item);

					ntfs_lookup_table_update(ntfs, lookup_table,
							dest_current_item, dest_current_item_index, false);

					ntfs_reallocate(ntfs, dest_current_item, dest_current_item_index,
							next_cluster_to_process + next_item_cluster_count);

					ntfs_lookup_table_update(
							ntfs, lookup_table, dest_current_item, dest_current_item_index, true);
				}
			}

			// nakopírujeme cluster a ten starý uvolníme
			int32_t src_cluster = next_item_clusters[i];
			io_copy_cluster(ntfs, src_cluster, dest_cluster);

			ntfs_bitmap_set(ntfs->bitmap, src_cluster, false);
			ntfs_bitmap_set(ntfs->bitmap, dest_cluster, true);
			lookup_table[dest_cluster] = next_item;
			lookup_table[src_cluster] = NULL;
		}

		// smazání ostatních mft položek, vyčištění fragmentů první položky
		ntfs_clean_content(ntfs, next_item, next_item_index, false);
		// nastavení nového fragmentu
		next_item->item_size = next_item_size;
		next_item->fragments[0].start_index = next_cluster_to_process;
		next_item->fragments[0].count = next_item_cluster_count;

		next_cluster_to_process += next_item_cluster_count;

		free(next_item_clusters);
	}

	free(lookup_table);
	return RESULT_OK;
}

Result ntfs_init_reallocation(NTFS* ntfs, MftItem* first_item, int32_t size_bytes,
	int32_t preferred_min_cluster_index,
	MftFragment** out_fragments, int32_t* out_fragment_count) {

	// nalezení volných clusterů
	MftFragment* fragments;
	int32_t fragment_count = 0;
	if (!ntfs_allocate_clusters(ntfs, size_bytes, preferred_min_cluster_index, &fragments, &fragment_count)) {
		return RESULT_WARNING;
	}

	// nalezení volných položek v MFT
	//  podle počtu fragmentů se určí počet mft items
	int mft_item_count = utils_div_ceil(fragment_count, NTFS_MFT_FRAGMENTS_COUNT);
	int free_mft_item_count = utils_max(0, mft_item_count - 1);
	MftItem **free_mft_items = (MftItem**)malloc((free_mft_item_count) * sizeof(MftItem*));
	if (ntfs_mft_find_free_mft_items(ntfs->mft, 0, free_mft_item_count, free_mft_items) != free_mft_item_count) {
		ntfs_debug("NOT ENOUGH MFT ITEMS\n");

		// zpětné uvolnění
		ntfs_release_clusters(ntfs, fragments, fragment_count);

		free(free_mft_items);
		return RESULT_WARNING;
	}

	// kopírování je možné provést

	// update MFT
	for (int i = 0; i < mft_item_count; ++i) {
		MftItem* item = NULL;

		if (i == 0) {
			// první položka
			item = first_item;
			item->item_size = size_bytes;
		}
		else {
			item = free_mft_items[i - 1];
			assert(item != first_item);

			item->uid = first_item->uid;
			item->attributes = first_item->attributes;
		}
		item->item_order = i;
		item->item_total = mft_item_count;

		// kopírování příslušných fragmentů
		int32_t done = (i * NTFS_MFT_FRAGMENTS_COUNT);
		int32_t remaining = fragment_count - done;
		int32_t to_copy = utils_min(NTFS_MFT_FRAGMENTS_COUNT, remaining);
		for (int j = 0; j < to_copy; ++j) {
			item->fragments[j].count = fragments[done + j].count;
			item->fragments[j].start_index = fragments[done + j].start_index;
		}
	}

	free(free_mft_items);
	*out_fragments = fragments;
	*out_fragment_count = fragment_count;
	return RESULT_OK;
}

Result ntfs_reallocate(NTFS* ntfs, MftItem* first_item, int32_t first_item_index, int32_t preferred_min_cluster_index) {
	int32_t size_bytes = first_item->item_size;

	// získání indexů zdrojových clusterů
	int32_t cluster_count = ntfs_count_clusters(ntfs, first_item);
	int32_t *src_clusters = (int32_t*)malloc((cluster_count) * sizeof(int32_t));
	ntfs_collect_clusters_from_mft_items(ntfs, first_item, first_item_index, src_clusters);

	// smazání ostatních mft položek, vyčištění fragmentů první položky
	ntfs_clean_content(ntfs, first_item, first_item_index, false);

	// alokace nových clusterů, nastavení fragmentů a mft dalších mft položek
	MftFragment *fragments;
	int32_t fragment_count;
	Result initialization = ntfs_init_reallocation(
		ntfs, first_item, size_bytes, preferred_min_cluster_index, &fragments, &fragment_count);

	if (initialization != RESULT_OK) {
		free(src_clusters);
		return RESULT_ERROR;
	}

	// přesun dat
	int32_t *dest_clusters = (int32_t*)malloc((cluster_count) * sizeof(int32_t));
	ntfs_collect_clusters_from_mft_items(ntfs, first_item, first_item_index, dest_clusters);

	for (int i = 0; i < cluster_count; ++i) {
		io_copy_cluster(ntfs, src_clusters[i], dest_clusters[i]);

		// uvolnění starého clusteru
		ntfs_bitmap_set(ntfs->bitmap, src_clusters[i], false);
	}

	free(fragments);
	free(src_clusters);
	free(dest_clusters);
	return RESULT_OK;
}

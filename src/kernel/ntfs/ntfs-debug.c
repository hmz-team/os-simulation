
/**
 * Implementuje debug funkce.
 *
 * @author: Patrik Harag
 * @version: 2018-10-24
 */

#include <stdarg.h>
#include <stdio.h>
#include "cpp-bridge.h"
#include "ntfs-debug.h"


void ntfs_debug(char const* format, ...) {
#ifdef _DEBUG
	va_list arg;
	va_start(arg, format);
	{
		char buffer[DEBUG_PRINT_BUFFER_SIZE];
		vsprintf_s(buffer, DEBUG_PRINT_BUFFER_SIZE, format, arg);

		cpp_bridge_print(buffer);
	}
	va_end(arg);
#endif
}


/**
 * Implementuje funkce pro práci s IO.
 *
 * @author: Patrik Harag
 * @version: 2018-10-27
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "cpp-bridge.h"
#include "ntfs-io.h"
#include "ntfs-module-find.h"
#include "ntfs-module-allocation.h"
#include "ntfs-debug.h"


static bool io_disk_write_data(NTFS *ntfs, int32_t offset_in_bytes, void *data, int32_t bytes_to_write) {
	if (bytes_to_write == 0) return true;
	bool status_ok = true;

	int32_t sector_size = ntfs->disk_sector_size;
	int32_t remaining_bytes = bytes_to_write;
	int32_t data_offset = 0;

	int32_t current_sector = offset_in_bytes / sector_size;
	int32_t first_sector_index = offset_in_bytes % sector_size;
	if (first_sector_index != 0) {
		// místo, kam chceme zapsat začíná/je uvnitř sektoru
		// - při zápisu nechceme smazat okolní data
		int32_t data_to_write_to_this_sector = utils_min(sector_size - first_sector_index, bytes_to_write);

		char *buffer = (char*)calloc(sector_size, sizeof(char));
		status_ok = cpp_bridge_read_sectors(ntfs, current_sector, buffer, 1);
		if (status_ok) {
			memcpy(buffer + first_sector_index, data, data_to_write_to_this_sector);
			status_ok = cpp_bridge_write_sectors(ntfs, current_sector, buffer, 1);

			remaining_bytes -= data_to_write_to_this_sector;
			data_offset += data_to_write_to_this_sector;
			current_sector++;
		}
		free(buffer);
	}

	// zapíšeme celé sektory (pokud jsou)
	if (status_ok && remaining_bytes > sector_size) {
		int32_t number_of_full_sectors = remaining_bytes / sector_size;
		status_ok = cpp_bridge_write_sectors(ntfs, current_sector, ((char *)data) + data_offset, number_of_full_sectors);

		current_sector += number_of_full_sectors;
		remaining_bytes -= number_of_full_sectors * sector_size;
		data_offset += number_of_full_sectors * sector_size;
	}

	// zapíšeme zbytek (pokud je) - nechceme při tom přepsat jiná data
	if (status_ok && remaining_bytes > 0) {
		char *buffer = (char*)calloc(sector_size, sizeof(char));
		status_ok = cpp_bridge_read_sectors(ntfs, current_sector, buffer, 1);
		if (status_ok) {
			memcpy(buffer, ((char *)data) + data_offset, remaining_bytes);
			status_ok = cpp_bridge_write_sectors(ntfs, current_sector, buffer, 1);
		}
		free(buffer);
	}

	return status_ok;
}

static bool io_disk_read_data(NTFS *ntfs, int32_t offset_in_bytes, void *data, int32_t bytes_to_read) {
	if (bytes_to_read == 0) return true;
	bool status_ok = true;

	int32_t sector_size = ntfs->disk_sector_size;
	int32_t out_index = 0;

	int32_t current_sector = offset_in_bytes / sector_size;
	int32_t first_sector_index = offset_in_bytes % sector_size;
	if (first_sector_index != 0) {
		// místo, které chceme načíst začíná/je uvnitř sektoru
		// - musíme si pomoci dalším bufferem

		int32_t data_to_read_from_this_sector = utils_min(sector_size - first_sector_index, bytes_to_read);

		char *buffer = (char*)calloc(sector_size, sizeof(char));
		status_ok = cpp_bridge_read_sectors(ntfs, current_sector, buffer, 1);
		if (status_ok) {
			memcpy(data, buffer + first_sector_index, data_to_read_from_this_sector);

			out_index += data_to_read_from_this_sector;
			current_sector++;
		}
		free(buffer);
	}

	// načteme celé sektory (pokud jsou)
	if (status_ok && (bytes_to_read - out_index) > sector_size) {
		int32_t number_of_full_sectors = (bytes_to_read - out_index) / sector_size;
		status_ok = cpp_bridge_read_sectors(ntfs, current_sector, ((char *)data) + out_index, number_of_full_sectors);

		current_sector += number_of_full_sectors;
		out_index += number_of_full_sectors * sector_size;
	}

	// načteme zbytek (pokud je)
	if (status_ok && (bytes_to_read - out_index) > 0) {
		char *buffer = (char*)calloc(sector_size, sizeof(char));
		status_ok = cpp_bridge_read_sectors(ntfs, current_sector, buffer, 1);
		if (status_ok) {
			memcpy(((char*)data) + out_index, buffer, bytes_to_read - out_index);
		}
		free(buffer);
	}

	return status_ok;
}

bool io_disk_format(NTFS* ntfs) {
	assert(ntfs->initialized == true);
	return io_update_boot_record(ntfs) & io_update_mft(ntfs) & io_update_bitmap(ntfs);
}

bool io_disk_try_load(NTFS* ntfs) {
	assert(ntfs->initialized == false);

	if (!io_disk_read_data(ntfs, 0, ntfs->boot_record, sizeof(BootRecord)))
		return false;

	if (strcmp(ntfs->boot_record->volume_descriptor, BOOT_RECORD_DESCRIPTOR) != 0)
		return false;  // asi není naformátovaný...

	ntfs_mft_init(ntfs->mft, ntfs->boot_record->mft_item_count);
	ntfs_bitmap_init(ntfs->bitmap, ntfs->boot_record->cluster_count);
	ntfs->initialized = true;

	return io_disk_read_data(ntfs, io_get_mft_section_start(ntfs), ntfs->mft->data, sizeof(MftItem) * ntfs->mft->length)
		& io_disk_read_data(ntfs, io_get_bitmap_section_start(ntfs), ntfs->bitmap->data, sizeof(char) * ntfs->bitmap->length);
}

int32_t io_file_read(NTFS* ntfs, MftItem* first_item, int32_t file_offset, void* out_buffer, int32_t out_buffer_size) {
	assert(ntfs->initialized == true);
	if (file_offset >= first_item->item_size) return 0;

	int32_t file_position = 0;
	int32_t bytes_read = 0;
	int32_t bytes_to_read = utils_min(out_buffer_size, first_item->item_size - file_offset);

	int32_t data_start = io_get_data_section_start(ntfs);

	int32_t last_mft_item_index = -1;
	for (int i = 0; i < first_item->item_total && bytes_to_read > 0; ++i) {
		MftItem* next_item = ntfs_find_next_mft_item(ntfs, first_item, i, &last_mft_item_index);
		if (next_item == NULL) {
			break;  // fs je poškozen
		}

		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT && bytes_to_read > 0; ++j) {
			MftFragment* fragment = &(next_item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) {
				break;  // tento fragment není použit - a nebudou oni ostatní, protože se alokují postupně
			}

			int32_t fragment_size_in_bytes = fragment->count * ntfs->boot_record->cluster_size;
			if (file_position + fragment_size_in_bytes < file_offset) {
				file_position += fragment_size_in_bytes;
				continue;  // přeskočení celého fragmentu
			}

			int32_t from = (file_position < file_offset) ? file_offset - file_position : 0;
			int32_t trimmed_size = utils_min(fragment_size_in_bytes - from, bytes_to_read);

			int32_t disk_offset = data_start + (fragment->start_index * ntfs->boot_record->cluster_size) + from;
			if (!io_disk_read_data(ntfs, disk_offset, ((char *)out_buffer) + bytes_read, trimmed_size)) {
				return bytes_read;  // čtení selhalo
			}

			bytes_read += trimmed_size;
			bytes_to_read -= trimmed_size;
			file_position += from + trimmed_size;
		}
	}

	return bytes_read;
}

char* io_file_read_all(NTFS* ntfs, MftItem* first_item) {
	char *data = (char*)malloc((first_item->item_size + 1) * sizeof(char));
	if (io_file_read(ntfs, first_item, 0, data, first_item->item_size) != first_item->item_size) {
		// error
		free(data);
		return NULL;
	}
	data[first_item->item_size] = '\0';
	return data;
}

int32_t io_file_write(NTFS* ntfs, MftItem* first_item, int32_t file_offset, void* data, int32_t data_size) {
	assert(ntfs->initialized == true);
	
	// pokud se obsah nevejde, alokujeme další clustery / mft
	if (file_offset + data_size > first_item->item_size) {
		ntfs_extend_file(ntfs, first_item, file_offset + data_size - first_item->item_size);
	}

	int32_t file_position = 0;
	int32_t bytes_written = 0;
	int32_t bytes_to_write = utils_min(data_size, first_item->item_size - file_offset);

	int32_t data_start = io_get_data_section_start(ntfs);

	int32_t last_mft_item_index = -1;
	for (int i = 0; i < first_item->item_total && bytes_to_write > 0; ++i) {
		MftItem* next_item = ntfs_find_next_mft_item(ntfs, first_item, i, &last_mft_item_index);
		if (next_item == NULL) {
			break;  // fs je poškozen
		}

		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT && bytes_to_write > 0; ++j) {
			MftFragment* fragment = &(next_item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) {
				break;  // tento fragment není použit - a nebudou oni ostatní, protože se alokují postupně
			}

			int32_t fragment_size_in_bytes = fragment->count * ntfs->boot_record->cluster_size;
			if (file_position + fragment_size_in_bytes < file_offset) {
				file_position += fragment_size_in_bytes;
				continue;  // přeskočení celého fragmentu
			}

			int32_t from = (file_position < file_offset) ? file_offset - file_position : 0;
			int32_t trimmed_size = utils_min(fragment_size_in_bytes - from, bytes_to_write);

			int32_t disk_offset = data_start + (fragment->start_index * ntfs->boot_record->cluster_size) + from;
			if (!io_disk_write_data(ntfs, disk_offset, ((char *)data) + bytes_written, trimmed_size)) {
				return bytes_written;
			}

			bytes_written += trimmed_size;
			bytes_to_write -= trimmed_size;
			file_position += from + trimmed_size;
		}
	}

	return bytes_written;
}

int32_t io_file_write_append(NTFS* ntfs, MftItem* first_item, void* data, int32_t data_size) {
	return io_file_write(ntfs, first_item, first_item->item_size, data, data_size);
}

bool io_copy_cluster(NTFS* ntfs, int32_t src_cluster_index, int32_t dest_cluster_index) {
	int32_t data_start = io_get_data_section_start(ntfs);

	int32_t cluster_size = ntfs->boot_record->cluster_size;
	char *buffer = (char*)malloc((cluster_size) * sizeof(char));

	int32_t src_cluster_start = src_cluster_index * cluster_size;
	if (!io_disk_read_data(ntfs, data_start + src_cluster_start, buffer, cluster_size)) {
		free(buffer);
		return false;
	}

	int32_t dest_cluster_start = dest_cluster_index * cluster_size;
	bool result = io_disk_write_data(ntfs, data_start + dest_cluster_start, buffer, cluster_size);
	free(buffer);
	return result;
}

int32_t io_get_mft_section_start(NTFS* ntfs) {
	return (int32_t) sizeof(BootRecord);
}

int32_t io_get_bitmap_section_start(NTFS* ntfs) {
	return io_get_mft_section_start(ntfs) + (int32_t) sizeof(MftItem) * ntfs->boot_record->mft_item_count;
}

int32_t io_get_data_section_start(NTFS* ntfs) {
	return io_get_bitmap_section_start(ntfs) + ntfs->boot_record->bitmap_size;
}

bool io_update_boot_record(NTFS* ntfs) {
	return io_disk_write_data(ntfs, 0, ntfs->boot_record, sizeof(BootRecord));
}

bool io_update_mft(NTFS* ntfs) {
	return io_disk_write_data(ntfs, io_get_mft_section_start(ntfs), ntfs->mft->data, sizeof(MftItem) * ntfs->mft->length);
}

bool io_update_bitmap(NTFS* ntfs) {
	return io_disk_write_data(ntfs, io_get_bitmap_section_start(ntfs), ntfs->bitmap->data, sizeof(char) * ntfs->bitmap->length);
}

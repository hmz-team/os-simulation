
/**
 * Implementuje funkce pro vyhledávání MFT položek.
 *
 * @author: Patrik Harag
 * @version: 2018-10-27
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "ntfs-module-find.h"
#include "ntfs-io.h"
#include "ntfs-debug.h"
#include "cpp-bridge.h"


MftItem* ntfs_find_root(NTFS* ntfs, int32_t* out_index) {
	assert(ntfs->initialized);

	Mft* mft = ntfs->mft;
	for (int i = 0; i < mft->length; ++i) {
		MftItem* item = &(mft->data[i]);
		if (item->uid != NTFS_MFT_UID_ITEM_FREE
				&& item->attributes & NTFS_ATTRIBUTE_DIRECTORY
				&& strcmp(item->item_name, "/") == 0) {

			if (out_index != NULL) {
				*out_index = i;
			}
			return item;
		}
	}
	return NULL;
}

MftItem* ntfs_find_item_by_uid(NTFS* ntfs, int32_t uid, int32_t from_index,
							  int32_t *out_index) {

	for (int i = from_index; i < ntfs->mft->length; ++i) {
		MftItem* item = &(ntfs->mft->data[i]);
		if (item->uid == uid) {
			if (out_index != NULL) {
				*out_index = i;
			}
			return item;
		}
	}
	return NULL;
}

int32_t ntfs_find_item_index_if_needed(NTFS* ntfs, MftItem* item) {
	if (item->item_total == 1) {
		return -1;  // není potřeba
	}
	int32_t index;
	ntfs_find_item_by_uid(ntfs, item->uid, 0, &index);
	return index;
}

MftItem* ntfs_find_next_mft_item(NTFS* ntfs, MftItem *first_item, int32_t i, int32_t *last_mft_item_index) {
	if (i == 0) {
		// první položku už máme, ale potřebujeme i index
		if (*last_mft_item_index == -1) {
			ntfs_find_item_by_uid(ntfs, first_item->uid, 0, last_mft_item_index);
		}
		return first_item;
	} else {
		assert(*last_mft_item_index != -1);
		MftItem *next_item = ntfs_find_item_by_uid(ntfs, first_item->uid, *last_mft_item_index + 1, last_mft_item_index);
		if (next_item == NULL) {
			ntfs_debug("Cannot find related MFT items, uid=%d\n", first_item->uid);
		}
		return next_item;
	}
}

MftFragment* ntfs_find_last_not_empty_mft_item(NTFS *ntfs, MftItem *first_item) {
	MftFragment *last_non_empty_fragment = NULL;
	int32_t last_mft_item_index = -1;
	for (int i = 0; i < first_item->item_total; ++i) {
		MftItem* next_item = ntfs_find_next_mft_item(ntfs, first_item, i, &last_mft_item_index);
		if (next_item == NULL) {
			break;  // fs je poškozen
		}

		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
			MftFragment *fragment = &(next_item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) {
				break;  // tento fragment není použit - a nebudou oni ostatní, protože se alokují postupně
			}
			else {
				last_non_empty_fragment = fragment;
			}
		}
	}
	return last_non_empty_fragment;
}

MftItem* ntfs_find_with_index(NTFS* ntfs, char* path, MftItem* current_dir,
							 int32_t* out_item_index) {

	if (strlen(path) == 0) {
		// lomítko na konci: xx/yy/ == xx/yy
		return current_dir;
	}

	const char *ptr = strchr(path, '/');
	if (ptr) {
		int index = (int) (ptr - path);

		if (index == 0) {
			// absolutní cesta
			return ntfs_find_with_index(ntfs, path+1, ntfs_find_root(ntfs, out_item_index), out_item_index);
		} else {
			// získáme z cesty první část

			char *first_part = (char*) malloc((index + 1) * sizeof(char));
			first_part[index] = '\0';
			memcpy(first_part, path, (size_t) index);

			MftItem* next_item = ntfs_find_in_dir(ntfs, first_part, current_dir);
			free(first_part);
			if (next_item == NULL || !(next_item->attributes & NTFS_ATTRIBUTE_DIRECTORY)) {
				return NULL;
			}

			return ntfs_find_with_index(ntfs, path + index + 1, next_item, out_item_index);
		}
	} else {
		// neobsahuje '/' => jeden název
		return ntfs_find_in_dir_with_index(ntfs, path, current_dir, out_item_index);
	}
}

MftItem* ntfs_find(NTFS* ntfs, char* path, MftItem* current_dir) {
	return ntfs_find_with_index(ntfs, path, current_dir, NULL);
}

MftItem* ntfs_find_parent(NTFS* ntfs, char* path, MftItem* current_dir, char* out_name) {
	if (strlen(path) == 0) {
		return NULL;
	}

	const char *ptr = strchr(path, '/');
	if (ptr) {
		int index = (int) (ptr - path);

		if (index == 0) {
			// absolutní cesta
			return ntfs_find_parent(ntfs, path+1, ntfs_find_root(ntfs, NULL), out_name);
		} else {
			// získáme z cesty první část

			char *first_part = (char*) malloc((index + 1) * sizeof(char));
			first_part[index] = '\0';
			memcpy(first_part, path, (size_t) index);

			MftItem* next_item = ntfs_find_in_dir(ntfs, first_part, current_dir);
			free(first_part);
			if (next_item == NULL || !(next_item->attributes & NTFS_ATTRIBUTE_DIRECTORY)) {
				return NULL;
			}

			return ntfs_find_parent(ntfs, path + index + 1, next_item, out_name);
		}
	} else {
		// neobsahuje '/' => jeden název

		size_t len = strlen(path);
		memcpy(out_name, path, (size_t) len);
		out_name[len] = '\0';

		return current_dir;
	}
}

MftItem* ntfs_find_in_dir_with_index(NTFS *ntfs, char *name, MftItem *dir, int32_t *out_item_index) {
	assert(dir->attributes & NTFS_ATTRIBUTE_DIRECTORY);

	if (dir->item_size == 0) {
		// adresář je prázdný
		return NULL;
	}

	// načtení seznamu souborů
	char *data = io_file_read_all(ntfs, dir);
	if (data == NULL) {
		return NULL;
	}

	int entries = dir->item_size / 4;
	for (int i = 0; i < entries; ++i) {
		int32_t uid = ((int32_t*) data)[i];

		MftItem* next_item = ntfs_find_item_by_uid(ntfs, uid, 0, out_item_index);
		if (next_item != NULL) {
			if (strcmp(name, next_item->item_name) == 0) {
				free(data);
				return next_item;
			}
		}
	}
	free(data);
	return NULL;
}

MftItem* ntfs_find_in_dir(NTFS* ntfs, char* name, MftItem* dir) {
	return ntfs_find_in_dir_with_index(ntfs, name, dir, NULL);
}

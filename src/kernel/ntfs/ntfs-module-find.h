#pragma once

/**
 * Definuje funkce pro vyhledávání MFT položek.
 *
 * @author: Patrik Harag
 * @version: 2018-10-27
 */

#include "ntfs.h"


/** Najde kořenovou složku */
MftItem* ntfs_find_root(NTFS* ntfs, int32_t* out_index);

/**
 * Najde první položku od určitého indexu v MFT podle UID.
 *
 * @param ntfs
 * @param uid
 * @param from_index index na kterém za začne hledat
 * @param out_index pointer kterému bude předán index nalezené položky,
 *				  může být NULL
 * @return první položka nebo NULL
 */
MftItem* ntfs_find_item_by_uid(NTFS* ntfs, int32_t uid, int32_t from_index, int32_t *out_index);

/**
 * Pokud je položka složena z více MFT položek, tak vrátí index první položky.
 * Jinak vrátí -1. Pokud totiž procházíme položky, tak potřebujeme znát index
 * té předchozí.
 *
 * @param ntfs
 * @param item první položka
 * @return index první položky
 */
int32_t ntfs_find_item_index_if_needed(NTFS* ntfs, MftItem* item);

MftItem* ntfs_find_next_mft_item(NTFS* ntfs, MftItem *first_item, int32_t i, int32_t *last_mft_item_index);

MftFragment* ntfs_find_last_not_empty_mft_item(NTFS *ntfs, MftItem *first_item);

/** Najde položku podle cesty */
MftItem* ntfs_find(NTFS* ntfs, char* path, MftItem* current_dir);

/** Najde položku podle cesty */
MftItem* ntfs_find_with_index(NTFS* ntfs, char* path, MftItem* current_dir, int32_t* out_item_index);

/**
 * Najde adresář, ve kterém je položka (nemusí existovat), na kterou ukazuje
 * zadaná cesta.
  *
  * @param ntfs
  * @param path
  * @param current_dir
  * @param out_name pointer na řetězec, do kterého umístí název položky
  * @return adresář
  */
MftItem* ntfs_find_parent(NTFS* ntfs, char* path, MftItem* current_dir, char* out_name);

/** Najde položku v daném adresáři podle jména */
MftItem* ntfs_find_in_dir(NTFS* ntfs, char* name, MftItem* dir);

/** Najde položku v daném adresáři podle jména */
MftItem* ntfs_find_in_dir_with_index(NTFS *ntfs, char *name, MftItem *dir, int32_t *out_item_index);

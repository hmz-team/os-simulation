#pragma once

/**
 * Definuje funkce CLI.
 *
 * @author: Patrik Harag
 * @version: 2018-10-20
 */

#include "ntfs.h"
#include "ntfs-utils.h"

#define REPL_CONSOLE_OK	   0
#define REPL_CONSOLE_NO_INPUT 1
#define REPL_CONSOLE_TOO_LONG 2

#define REPL_PATH_MAX_LENGTH 128

extern bool REPL_TEST;

/** Struktura, která udržuje stav v jakém se nachází REPL. */
typedef struct _ConsoleState {
	NTFS* ntfs;
	MftItem* dir;
} ConsoleState;

/** Vyhodnotí textový vstup, který obsahuje příkaz */
Result console_eval_command(ConsoleState* console_state, const char* input);
/** Vyhodnotí soubor s příkazy */
Result console_eval_script(ConsoleState* console_state, char* script_file);

/** Inicializuje strukturu */
void console_state_init(ConsoleState *console_state);
/** Uvolní strukturu */
void console_state_free(ConsoleState *console_state);

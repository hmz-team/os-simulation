#pragma once

/**
 * Definuje základní funkce pro práci s ntfs.
 *
 * @author: Patrik Harag
 * @version: 2018-10-28
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "ntfs-utils.h"
#include "ntfs-struct-boot-record.h"
#include "ntfs-struct-mft.h"
#include "ntfs-struct-bitmap.h"

#define NTFS_DEFAULT_DISK_SIZE (1024 * 10)
#define NTFS_DEFAULT_CLUSTER_SIZE 256

// struktury

/** Uchovává všechny důležité informace o ntfs */
typedef struct _NTFS {
	uint8_t disk_number;
	int32_t disk_sector_size;
	uint8_t disk_last_error;

	bool initialized;

	BootRecord* boot_record;
	Mft* mft;
	Bitmap* bitmap;
} NTFS;

// funkce

/** Vytvoří struktutu, kterou je nutné ještě inicializovat */
NTFS* ntfs_create(uint8_t disk_number, int32_t disk_sector_size);
/** Uvolní strukturu */
void ntfs_free(NTFS* ntfs);

/** Inicializuje virtuální filesystem a vrátí kořen, pokud selže tak NULL */
MftItem* ntfs_init(NTFS* ntfs, int32_t disk_size, int32_t cluster_size, int32_t mft_size);
MftItem* ntfs_init_disk_of_total_size(NTFS* ntfs, int32_t total_size_bytes, int32_t cluster_size);
MftItem* ntfs_load_or_init_disk_of_total_size(NTFS* ntfs, int32_t total_size_bytes, int32_t cluster_size);
/** Vytvoří kořenový adresář */
MftItem* ntfs_create_root_dir(NTFS* ntfs);

/** Vytvoří soubor */
MftItem* ntfs_create_empty_file(NTFS* ntfs, char* name, MftItem* dir, uint8_t attributes);
MftItem* ntfs_create_file(NTFS* ntfs, char* name, void *data, int32_t size, MftItem* dir);

/** Odstraní soubor nebo složku */
void ntfs_remove(NTFS* ntfs, MftItem* first_item, int32_t first_item_index, MftItem* dir);

/** Přidá položku do složky */
bool ntfs_add_item_into_dir(NTFS *ntfs, MftItem *dir, int32_t uid);
/** Odebere položku ze složky */
Result ntfs_remove_item_from_dir(NTFS *ntfs, MftItem *dir, int32_t uid);

/** Smaže obsah položky, první položku zanechá */
void ntfs_clean_content(NTFS* ntfs, MftItem* first_item, int32_t first_item_index, bool change_bitmap);
/** Smaže jednu MFT položku */
void ntfs_clean_mft_item(NTFS* ntfs, MftItem* item, bool full, bool change_bitmap);

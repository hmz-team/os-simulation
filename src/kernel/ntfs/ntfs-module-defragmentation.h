#pragma once

/**
 * Definuje funkce pro defragmentaci.
 *
 * @author: Patrik Harag
 * @version: 2018-10-28
 */

#include "ntfs.h"


/**
 * Provede úplnou defragmentaci ntfs.
 *
 * @param ntfs virtuální fs
 * @return výsldek operace
 */
Result ntfs_defragment(NTFS* ntfs);

/** Realokuje soubor */
Result ntfs_reallocate(NTFS* ntfs, MftItem* first_item, int32_t first_item_index, int32_t preferred_min_cluster_index);

/** Připraví mft pro vytvoření nového souboru realokací */
Result ntfs_init_reallocation(NTFS* ntfs, MftItem* first_item, int32_t size_bytes,
	int32_t preferred_min_cluster_index, MftFragment** out_fragments, int32_t* out_fragment_count);


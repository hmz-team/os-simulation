#pragma once

/**
 * Definuje různé validační funkce.
 *
 * @author: Patrik Harag
 * @version: 2018-10-20
 */

#include "ntfs.h"


/** Zkontroluje jméno položky. */
bool ntfs_check_name(char* name);
/** Zkontroluje zda je možné vytvořit nový soubor */
bool ntfs_check_new_item(NTFS* ntfs, MftItem* new_item_dir, char* new_item_name);

#pragma once

/**
 * Definuje různé pomocné funkce pro práci s ntfs.
 *
 * @author: Patrik Harag
 * @version: 2018-10-20
 */

#include "ntfs.h"

/** Sesbírá všechny MFT položky jedenoho souboru nebo složky dojednoho pole */
void ntfs_collect_mft_items(NTFS* ntfs, MftItem* first_item, int32_t first_item_index, MftItem** out_mft_items);
/** Sesbírá všechny clustery jedenoho souboru nebo složky dojednoho pole */
void ntfs_collect_clusters_from_mft_items(NTFS* ntfs, MftItem* first_item, int32_t first_item_index, int32_t* out_cluster_ids);
/** Sesbírá všechny clustery jedenoho souboru nebo složky dojednoho pole */
void ntfs_collect_clusters_from_fragments(MftFragment* fragments, int fragment_count, int32_t* out_cluster_ids);

/** Spočítá počet clusterů které zabírá jeden soubor nebo složka */
int32_t ntfs_count_clusters(NTFS* ntfs, MftItem* item);
/** Spočítá počet clusterů které zabírá jeden soubor nebo složka, skutečně je projde */
int32_t ntfs_count_clusters_real(NTFS* ntfs, MftItem* item);

/** Spočítá volné místo */
int32_t ntfs_count_free_space(NTFS* ntfs);

/** Vytvoří tabulku clusterů: index => MftItem*  */
void ntfs_lookup_table_fill(NTFS* ntfs, MftItem** out_table);
/** Aktualizuje tabulku o určitou položku */
void ntfs_lookup_table_update(NTFS* ntfs, MftItem** out_table,
							 MftItem* first_item, int32_t first_item_index, bool used);

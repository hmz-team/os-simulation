#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "ntfs.h"

static const uint16_t NTFS_ERROR_CODE_SUCCESS = 0;
static const uint16_t NTFS_ERROR_CODE_INVALID_ARGUMENT = 1;
static const uint16_t NTFS_ERROR_CODE_FILE_NOT_FOUND = 2;
static const uint16_t NTFS_ERROR_CODE_DIRECTORY_NOT_EMPTY = 3;
static const uint16_t NTFS_ERROR_CODE_NOT_ENOUGH_DISK_SPACE = 4;
static const uint16_t NTFS_ERROR_CODE_OUT_OF_MEMORY = 5;
static const uint16_t NTFS_ERROR_CODE_PERMISSION_DENIED = 6;
static const uint16_t NTFS_ERROR_CODE_IO_ERROR = 7;
static const uint16_t NTFS_ERROR_CODE_UNKNOWN_ERROR = -1;

static const uint8_t NTFS_ATTRIBUTE_DIRECTORY = 0x10;


bool cpp_bridge_write_sectors(NTFS *ntfs, int32_t offset, void *data, int32_t number_of_sectors);
bool cpp_bridge_read_sectors(NTFS *ntfs, int32_t offset, void *data, int32_t number_of_sectors);
void cpp_bridge_print(char* str);

#ifdef __cplusplus
}
#endif
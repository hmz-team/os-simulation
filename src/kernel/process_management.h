#pragma once

#include "..\api\api.h"
#include "signal_management.h"
#include <vector>
#include <thread>
#include <map>
#include <Windows.h>
#include <atomic>

class TCB {
public:
	kiv_os::THandle pid;
	std::string id;
	std::shared_ptr<std::thread> thread;
};

class PCB {
public:
	PCB();
	~PCB();
	const char *name;
	kiv_os::THandle pid;
	std::atomic<uint16_t> exit_code;
	std::vector<std::shared_ptr<TCB>> threads;
};

class Process_Management {
private:
	std::vector<std::shared_ptr<PCB>> process_table;
	std::map<std::string, std::shared_ptr<TCB>> thread_table;
	std::shared_ptr<Signal_Management> signals;
	uint16_t count = 0;

public:
	bool system_terminated;
	Process_Management();
	~Process_Management();
	void handle_process(kiv_hal::TRegisters &regs, HMODULE User_Programs);
	kiv_os::THandle create_process(kiv_hal::TRegisters &regs, HMODULE User_Programs);
	void terminate_process(kiv_os::NOS_Error err);
	void create_thread(size_t pid, kiv_os::TThread_Proc proc, kiv_hal::TRegisters & regs, bool create_thread_syscall);
	void read_exit_code(kiv_hal::TRegisters & regs);
	void wait_for_first_program();
	size_t get_pid();
};
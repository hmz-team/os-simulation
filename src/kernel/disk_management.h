#pragma once

#include "..\api\hal.h"
#include "..\api\api.h"

extern "C" {
	#include "ntfs/ntfs.h"
}

// Manages one disk
class Disk_Management {
	private:
		uint8_t disk_drive_number;
		kiv_hal::TDrive_Parameters drive_parameters;

		NTFS* ntfs;
		MftItem* root_dir;
		MftItem* debug_working_dir;

		void Clear_Error_Code();

	public:
		Disk_Management(uint8_t disk_drive_number, kiv_hal::TDrive_Parameters drive_parameters);
		~Disk_Management();

		void Run_Debug_Command(const char* command);

		// vr�t� ko�enov� adres��
		MftItem* Get_Root_Dir();
		// najde soubor, path m��e b�t absolutn� i relativn� cesta (v��i working_dir)
		// pokud nic nenajde, vrac� NULL
		MftItem* Find_File(char *path, MftItem *working_dir);
		MftItem* Find_File(int32_t uid);
		// vytvo�� soubor s dan�m n�zvem
		MftItem* Create_File(char *name, MftItem *dir, uint8_t attributes, kiv_os::NOS_Error *out_error_code);
		// zap�e bajty do souboru
		// zapisovat do adres��e je zak�z�no
		int32_t Write(MftItem *file, int32_t offset_in_bytes, void* data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code);
		// na�te bajty ze souboru
		// POZOR: adres�� obsahuje pro ka�d� soubor uid typu int32_t
		int32_t Read(MftItem *file, int32_t offset_in_bytes, void* data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code);

		uint32_t Get_Total_Size();
};

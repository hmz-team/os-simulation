#pragma once

#include <map>
#include <memory>
#include "../api/api.h"

class Descriptor {
public:
	
	char* file;
	virtual void write(const char* buff, size_t buff_len) = 0;
	//TODO read
};

class File_Desc_Management {
private:
	std::map<uint16_t, std::shared_ptr<Descriptor>> table;
	kiv_os::THandle counter = 2;
public:
	File_Desc_Management();
	void write(kiv_os::THandle handle, const char* buff, size_t buff_len);
	void load_file(const char* file);
	//TODO read
};
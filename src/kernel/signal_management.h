#pragma once
#include "../api/api.h"
#include <memory>
#include <vector>

class Signal {
public:
	const kiv_os::TThread_Proc handler;
	const uint8_t number;
	Signal(uint8_t signal_number, kiv_os::TThread_Proc handler);
};

class Signal_Management {
private:
	std::vector<std::shared_ptr<Signal>> registered_handlers;

public:
	Signal_Management();
	~Signal_Management();
	void handle_signal(uint8_t signal_number, kiv_hal::TRegisters & regs);
	void register_signal_handler(kiv_os::NSignal_Id signal_id, kiv_os::TThread_Proc handler);
	//TODO vymazat handler pro proces, ktery zavolal EXIT (jeste je treba tenhle syscall implementovat...)
};
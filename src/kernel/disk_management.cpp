#include "disk_management.h"
#include <memory>
#include <string>
#include <algorithm>

extern "C" {
	#include "ntfs/ntfs-io.h"
	#include "ntfs/ntfs-module-find.h"
	#include "ntfs/ntfs-module-validation.h"
	#include "ntfs/ntfs-console.h"
	#include "ntfs/ntfs-console-commands.h"
}

Disk_Management::Disk_Management(uint8_t disk_drive_number, kiv_hal::TDrive_Parameters drive_parameters) {
	this->disk_drive_number = disk_drive_number;
	this->drive_parameters = drive_parameters;

	this->ntfs = ntfs_create(disk_drive_number, drive_parameters.bytes_per_sector);
	// na�ten� boot recordu ze za��tku disku a podle n�j na�ten� ostatn�ch struktur
	// nebo jeho vytvo�en� a naform�tov�n� disku o uveden�ch parametrech
	this->root_dir = ntfs_load_or_init_disk_of_total_size(ntfs, /*getTotalSize()*/ 1024 * 5, /*drive_parameters.bytes_per_sector*/ 64);
	this->debug_working_dir = root_dir;

	Clear_Error_Code();
}

void Disk_Management::Run_Debug_Command(const char *command) {
	ConsoleState console_state;
	console_state.ntfs = this->ntfs;
	console_state.dir = this->debug_working_dir;
	console_eval_command(&console_state, command);
	this->debug_working_dir = console_state.dir;

	Clear_Error_Code();
}

MftItem* Disk_Management::Get_Root_Dir() {
	return root_dir;
}

MftItem* Disk_Management::Find_File(char *path, MftItem *working_dir) {
	MftItem* result = ntfs_find(ntfs, path, working_dir);
	Clear_Error_Code();
	return result;
}

MftItem* Disk_Management::Find_File(int32_t uid) {
	int32_t out_index = 0;
	MftItem* result = ntfs_find_item_by_uid(ntfs, uid, 0, &out_index);
	Clear_Error_Code();
	return result;
}

MftItem* Disk_Management::Create_File(char *name, MftItem *dir, uint8_t attributes, kiv_os::NOS_Error *out_error_code) {
	MftItem *item = ntfs_create_empty_file(ntfs, name, dir, attributes);

	*out_error_code = static_cast<kiv_os::NOS_Error>(ntfs->disk_last_error);
	Clear_Error_Code();

	return item;
}

int32_t Disk_Management::Write(MftItem *file, int32_t offset_in_bytes, void *data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) {
	if (file->attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
		// do adres��e se nep�e p��mo, od toho je Create_File
		*out_error_code = kiv_os::NOS_Error::Permission_Denied;
		return 0;
	}

	int32_t bytes_written = io_file_write(ntfs, file, offset_in_bytes, data, data_size_in_bytes);

	*out_error_code = static_cast<kiv_os::NOS_Error>(ntfs->disk_last_error);
	Clear_Error_Code();

	return bytes_written;
}

int32_t Disk_Management::Read(MftItem *file, int32_t offset_in_bytes, void *data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) {
	int32_t bytes_read = io_file_read(ntfs, file, offset_in_bytes, data, data_size_in_bytes);

	*out_error_code = static_cast<kiv_os::NOS_Error>(ntfs->disk_last_error);
	Clear_Error_Code();

	return bytes_read;
}

uint32_t Disk_Management::Get_Total_Size() {
	return (uint32_t) drive_parameters.absolute_number_of_sectors * drive_parameters.bytes_per_sector;
}

void Disk_Management::Clear_Error_Code() {
	ntfs->disk_last_error = static_cast<uint16_t>(kiv_os::NOS_Error::Success);
}

Disk_Management::~Disk_Management() {
	io_update_mft(ntfs);
	io_update_bitmap(ntfs);
	ntfs_free(ntfs);
}

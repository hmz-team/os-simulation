#include "process_management.h"
#include <memory>
#include <thread>
#include <algorithm>
#include <sstream>
#include <mutex>
#include <sstream>
#include "../user/leaks.h"

std::mutex m_process;
std::mutex m_exit;

Process_Management::Process_Management() {
	process_table = {};
	thread_table = {};
	signals = std::make_shared<Signal_Management>();
	system_terminated = false;
}

Process_Management::~Process_Management() {
	thread_table.clear();
}

void Process_Management::handle_process(kiv_hal::TRegisters & regs, HMODULE User_Programs)
{
	switch (static_cast<kiv_os::NOS_Process>(regs.rax.l)) {

	case kiv_os::NOS_Process::Clone:
		switch (static_cast<kiv_os::NClone>(regs.rcx.l)) {
		case kiv_os::NClone::Create_Process: {
			m_process.lock();

			kiv_os::THandle handle = create_process(regs, User_Programs);
			regs.rax.x = static_cast<decltype(regs.rax.x)>(handle);

			m_process.unlock();
			break;
		}

		case kiv_os::NClone::Create_Thread:
			m_process.lock();
			create_thread(get_pid(), reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r), regs, true);

			m_process.unlock();
			break;
		}

		break;


	case kiv_os::NOS_Process::Register_Signal_Handler:

		signals->register_signal_handler(static_cast<kiv_os::NSignal_Id>(regs.rcx.l), reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r));
		break;

	case kiv_os::NOS_Process::Exit: {
		kiv_os::NOS_Error err = static_cast<kiv_os::NOS_Error>(regs.rcx.x);
		terminate_process(err);
		break;
	}

	case kiv_os::NOS_Process::Read_Exit_Code: {
		read_exit_code(regs);
		break;
	}

	case kiv_os::NOS_Process::Shutdown:

		signals->handle_signal(static_cast<uint8_t>(kiv_os::NSignal_Id::Terminate), regs);
		break;
	}
}

kiv_os::THandle Process_Management::create_process(kiv_hal::TRegisters & regs, HMODULE User_Programs) {
	char* program = reinterpret_cast<char*>(regs.rdx.r);
	kiv_hal::TRegisters new_regs;

	kiv_os::THandle in = (uint16_t)((regs.rbx.e & 0xFFFF0000) >> 16);
	kiv_os::THandle out = (uint16_t)(regs.rbx.e & 0x0000FFFF);

	kiv_os::TThread_Proc proc = (kiv_os::TThread_Proc)GetProcAddress(User_Programs, program);

	new_regs.rax.x = static_cast<decltype(new_regs.rax.x)>(in);
	new_regs.rbx.x = static_cast<decltype(new_regs.rbx.x)>(out);
	new_regs.rdi.r = regs.rdi.r;

	auto pcb = std::make_shared<PCB>();
	pcb->name = program;
	pcb->pid = static_cast<kiv_os::THandle>(count++);
	pcb->exit_code = UINT16_MAX;

	process_table.push_back(pcb);

	m_exit.lock();
	create_thread(pcb->pid, proc, new_regs, false);
	m_exit.unlock();
	return pcb->pid;
}

void Process_Management::terminate_process(kiv_os::NOS_Error err) {
	
	m_exit.lock();
	size_t pid = get_pid();
	for (auto it = thread_table.begin(); it != thread_table.end(); ) {
		if (it->second->pid == pid) {

			if (pid > 0) { //pid 0 je prvni proces a na ten se ceka v kernelu pomoci join
				it->second->thread->detach();
				it = thread_table.erase(it);
			}

			if (pid == 0) {
				break;
			}
		}

		else {
			++it;
		}
	}

	for (auto it = process_table.begin(); it != process_table.end(); ) {
		PCB & pcb = **it;

		if (pcb.pid == pid) {
			pcb.threads.clear();
			pcb.exit_code = static_cast<uint16_t>(err);
			//process_table.erase(it);
			break;
		}

		else{
			++it;
		}
	}
	m_exit.unlock();
}

void Process_Management::create_thread(size_t pid, kiv_os::TThread_Proc proc, kiv_hal::TRegisters & regs, bool create_thread_syscall) {
	std::shared_ptr<std::thread> new_thread = nullptr;
	std::shared_ptr<TCB> tcb = std::make_shared<TCB>();

	for (auto & pcb : process_table) {

		if (pcb->pid == pid) {

			new_thread = std::make_shared<std::thread>(proc, regs);

			auto id = new_thread->get_id();
			std::ostringstream ss;
			ss << new_thread->get_id();
			tcb->id = ss.str();
			tcb->pid = pid;
			tcb->thread = new_thread;

			thread_table.insert({ tcb->id, tcb });

			pcb->threads.push_back(tcb);

			return;
		}
	}

	//TODO if syscall Create_Thread return handle in rax
}

void Process_Management::read_exit_code(kiv_hal::TRegisters & regs) {
	uint16_t pid = regs.rdx.x;
	
	for (auto it = process_table.begin(); it != process_table.end(); ) {
		PCB & pcb = **it;

		if (pcb.pid == pid) {

			while (pcb.exit_code == UINT16_MAX) {
				std::this_thread::yield();
			}

			m_exit.lock();
			regs.rcx.x = pcb.exit_code;
			process_table.erase(it);
			m_exit.unlock();
			break;
		}

		else {
			++it;
		}
	}
}

void Process_Management::wait_for_first_program() {
	process_table[0]->threads[0]->thread->join();
}

size_t Process_Management::get_pid()
{
	std::ostringstream ss;
	ss << std::this_thread::get_id();
	//return thread_table[ss.str()]->pid;
	return thread_table.find(ss.str())->second->pid;
}

PCB::PCB() { }

PCB::~PCB() { }

#pragma once

#include "..\api\api.h"
#include <vector>
#include <thread>
#include <Windows.h>
#include "process_management.h"

class TCB {
public:
	TCB();
	~TCB();
	const char *name;
	size_t tid;
	std::vector<std::thread> threads;
	//std::thread thread;
};

class Thread_Management {
private:
	std::vector<std::shared_ptr<PCB>> process_table;
	size_t count = 0;

public:
	Thread_Management();
	~Thread_Management();
};